    function clear() {
        localStorage.removeItem("firstDone");
        $.ajax({
            url: "deleteTimer",
            method: 'DELETE',
            dataType: 'json',
            success: (response, xhr) => {
                console.log("delete success");
                document.location.href = "./game";
            },
            error: (xhr) => {
                console.log('data transfert error' + xhr);
            }
        });
    }

    function getTargetTime() {
        let startError = false;
        if (!("firstDone" in localStorage)) {
            $.ajax({
                url: "startTimer",
                method: 'POST',
                dataType: 'json',
                success: (data) => {
                    console.log("init timer", data);
                },
                error: (error) => {
                    console.log('data transfert error', error);
                    startError = true;
                    setTimeout(clear(), 6000);
                    alert("Un problème est survenu au démarrage de la partie la partie va se relancer au scan de départ");
                }
            });
        }
        let dataRes = null;
        if (!startError) {
            $.ajax({
                url: "getTimer",
                method: 'GET',
                dataType: 'json',
                success: (response, xhr) => {
                    dataRes = response;
                    countdownManager.targetTime = response;
                    countdownManager.init();
                },
                error: (xhr) => {
                    console.log('data transfert error', xhr);
                    setTimeout(clear(), 6000);
                    alert("Un problème est survenu au démarrage de la partie la partie va se relancer au scan de départ");
                }
            });
        }
    }


    countdownManager = {
        // Configuration
        targetTime: null, // Date cible du compte à rebours (00:00:00)
        displayElement: { // Elements HTML où sont affichés les informations
            day: null,
            hour: null,
            min: null,
            sec: null
        },

        // Initialisation du compte à rebours (à appeler 1 fois au chargement de la page)
        init: function() {
            // Récupération des références vers les éléments pour l'affichage
            // La référence n'est récupérée qu'une seule fois à l'initialisation pour optimiser les performances
            this.displayElement.hour = jQuery('#countdown_hour');
            this.displayElement.min = jQuery('#countdown_min');
            this.displayElement.sec = jQuery('#countdown_sec');

            // Lancement du compte à rebours
            this.tick(); // Premier tick tout de suite
            window.setInterval("countdownManager.tick();", 1000); // Ticks suivant, répété toutes les secondes (1000 ms)
        },

        // Met à jour le compte à rebours (tic d'horloge)
        tick: function() {
            // Instant présent
            var timeNow = new Date();
            // On s'assure que le temps restant ne soit jamais négatif (ce qui est le cas dans le futur de targetTime)
            if (timeNow > this.targetTime) {
                timeNow = this.targetTime;
            }

            // Calcul du temps restant
            var diff = this.dateDiff(timeNow, this.targetTime);
            if ((diff.sec) <= 0 && (diff.min) <= 0 && (diff.hour) <= 0 && "firstDone" in localStorage) {
                localStorage.setItem("BadEnd", "Timeout:échec de la mission");
            } else {
                this.displayElement.hour.text(diff.hour);
                this.displayElement.min.text(diff.min);
                this.displayElement.sec.text(diff.sec);
            }
        },

        // Calcul la différence entre 2 dates, en jour/heure/minute/seconde
        dateDiff: function(date1, date2) {
            var diff = {} // Initialisation du retour
            var tmp = date2 - date1;

            tmp = Math.floor(tmp / 1000); // Nombre de secondes entre les 2 dates
            diff.sec = tmp % 60; // Extraction du nombre de secondes
            tmp = Math.floor((tmp - diff.sec) / 60); // Nombre de minutes (partie entière)
            diff.min = tmp % 60; // Extraction du nombre de minutes
            tmp = Math.floor((tmp - diff.min) / 60); // Nombre d'heures (entières)
            diff.hour = tmp % 24; // Extraction du nombre d'heures

            return diff;
        }
    }
    $(document).ready(() => {
        getTargetTime();
    });