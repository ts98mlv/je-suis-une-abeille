$(document).ready(() => {
    let nbPotsMiel = parseInt(localStorage.getItem("potDeMiel"));
    let nbPlantesTrouvees = parseInt(localStorage.getItem("nbPlantesTrouvees"));
    let allFind = false;
    if ("BadEnd" in localStorage) {
        $("#titleFin").append(localStorage.getItem("BadEnd"));
        $("#resume").append("<h3 class='text-center'>Vous avez dépassé le temps limite</h3>");
        $("#resume").append("<p>-Pot de miels: " + nbPotsMiel + "</p><p>-Nombre de plantes trouvées: " + nbPlantesTrouvees + "</p>");
    } else if ("GoodEnd" in localStorage) {
        $("#titleFin").append(localStorage.getItem("GoodEnd"));
        if (nbPlantesTrouvees === 36) {
            nbPotsMiel += 2;
            allFind = true;
        }
        $("#resume").append("<p>-Pot de miels: " + nbPotsMiel + "</p><p>-Nombre de plantes trouvées: " + nbPlantesTrouvees + "</p>");
        if (allFind) {
            $("#resume").append("<p>-Vous avez trouvé toutes les plantes mélifères du jardin</p>");
        }
    } else {
        $("#titleFin").append("Aie Aie Aie");
    }
    localStorage.removeItem("potDeMiel");
    localStorage.removeItem("nectar");
    localStorage.removeItem("pollen");
    localStorage.removeItem("GoodEnd");
    localStorage.removeItem("nbPlantes");
    localStorage.removeItem("nbPlantesTrouvees");
    localStorage.removeItem("tabPlanteTrouvees");
    localStorage.removeItem("starterCode");
});