/**
 * Mise a jour de l'utilisateur modifie dans la liste
 */
function refreshUser(params) {
    console.log("coucou");
    let ligneAMaj = $("tbody tr[data-id=" + params["idutilisateur"] + "]");
    ligneAMaj.children(".colonne-2").text(params["username"]);
    ligneAMaj.children(".colonne-3").text(params["lastname"]);
    ligneAMaj.children(".colonne-4").text(params["firstname"]);
    ligneAMaj.children(".colonne-5").text(params["email"]);
    ligneAMaj.children(".colonne-6").text(params["admin"]);
    if (params["admin"] == 1) {
        ligneAMaj.addClass("table-warning");
    } else {
        ligneAMaj.removeClass("table-warning");
    }
}

$(document).ready(function () {
    // Chargement de la modal edit avec les infos de l'user selectionne
    $('#editModal').on('show.bs.modal', function (event) {
        let ligne = $(event.relatedTarget).parents("tr");
        let modal = $(this);

        modal.find('.modal-title b').text("#" + ligne.data("id"));

        modal.find('#idutilisateur').val(ligne.data("id"));
        modal.find('#username').val(ligne.children(".colonne-2").text());

        modal.find('#firstname').val(ligne.children(".colonne-4").text());
        modal.find('#lastname').val(ligne.children(".colonne-3").text());

        modal.find('#email').val(ligne.children(".colonne-5").text());

        modal.find('#password').val("password");
        modal.find('#passwordrepeat').val("password");

        modal.find('#admin').val(ligne.children(".colonne-6").text());
    });

    // Chargement de la modal remove avec les infos de l'user selectionne
    $('#removeModal').on('show.bs.modal', function (event) {
        let ligne = $(event.relatedTarget).parents("tr");
        let modal = $(this);

        modal.find('.modal-title b').text("#" + ligne.data("id"));
        modal.find('#r_idutilisateur').val(ligne.data("id"));
        modal.find('#r_username').children("b").text(ligne.children(".colonne-2").text());
        modal.find('#r_firstname').children("b").text(ligne.children(".colonne-4").text());
        modal.find('#r_lastname').children("b").text(ligne.children(".colonne-3").text());
        modal.find('#r_email').children("b").text(ligne.children(".colonne-5").text());
    });

    // Init du modal pour ajout utilisateur
    $('#addModal').on('show.bs.modal', function (event) {
        let modal = $(this);

        modal.find('#a_idutilisateur').val("");
        modal.find('#a_username').val("");

        modal.find('#a_firstname').val("");
        modal.find('#a_lastname').val("");

        modal.find('#a_password').val("");
        modal.find('#a_passwordrepeat').val("");

        modal.find('#a_email').val("");

        modal.find('#a_admin').val(0);
    });
});