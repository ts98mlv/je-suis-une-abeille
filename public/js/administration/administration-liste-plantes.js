/**
 * Mise a jour de la plante modifie dans la liste
 */
function refreshPlante(params) {
    let ligneAMaj = $("tbody tr[data-id=" + params["plante_id"] + "]");
    ligneAMaj.children(".colonne-2").text(params['zone']);
    ligneAMaj.children(".colonne-3").text(params["nomFrancais"]);
    ligneAMaj.children(".colonne-4").text(params["nomLatin"]);
}

$(document).ready(function () {
    // Chargement de la modal edit avec les infos de la plante selectionnee
    $('#editModal').on('show.bs.modal', function (event) {
        let ligne = $(event.relatedTarget).parents("tr");
        let modal = $(this);

        modal.find('#edit_plante_id').val(ligne.data("id"));
        modal.find('#edit_nomFrancais').val(ligne.children(".colonne-3").text());
        modal.find('#edit_nomLatin').val(ligne.children(".colonne-4").text());
        modal.find('#edit_zone').val(ligne.children(".colonne-2").text());
        modal.find('#edit_description').val(ligne.data("description"));
        modal.find('#edit_hauteur').val(ligne.data("hauteur"));
        modal.find('#edit_nectar').val(ligne.data("nectar"));
        modal.find('#edit_pollen').val(ligne.data("pollen"));
        modal.find('#edit_miellat').val(ligne.data("miellat"));
        modal.find('#edit_floraisonCouleur').val(ligne.data("floraisoncouleur"));
    });

    // Chargement de la modal remove avec les infos de la plante selectionnee
    $('#removeModal').on('show.bs.modal', function (event) {
        let ligne = $(event.relatedTarget).parents("tr");
        let modal = $(this);

        modal.find('.modal-title b').text("#" + ligne.data("id"));
        modal.find('#r_plante_id').val(ligne.data("id"));
        modal.find('#r_nomFrancais').children("b").text(ligne.children(".colonne-3").text());
        modal.find('#r_nomLatin').children("b").text(ligne.children(".colonne-4").text());
        modal.find('#r_zone').children("b").text(ligne.children(".colonne-2").text());
    });


    // Init du modal pour ajout plante
    $('#addModal').on('show.bs.modal', function (event) {
        let modal = $(this);

        modal.find('#add_nomFrancais').val("");
        modal.find('#add_nomLatin').val("");
        modal.find('#add_zone').val("");
        modal.find('#add_description').val("");
        modal.find('#add_hauteur').val("1");
        modal.find('#add_nectar').val("1");
        modal.find('#add_pollen').val("1");
        modal.find('#add_miellat').val("1");
        modal.find('#add_floraisonCouleur').val("");
    });

    // Chargement des données d'une plante dans la modal
    $("#viewModal").on('show.bs.modal', function (event) {
        let ligne = $(event.relatedTarget).parents("tr");
        let modal = $(this);

        modal.find("#view_id").children("b").text(ligne.children(".colonne-1").text());
        modal.find("#view_nomFrancais").children("b").text(ligne.children(".colonne-3").text());
        modal.find("#view_nomLatin").children("b").text(ligne.children(".colonne-4").text());
        modal.find("#view_zone").children("b").text(ligne.children(".colonne-2").text());
        modal.find("#view_description").children("b").text(ligne.data("description"));
        modal.find("#view_hauteur").children("b").text(ligne.data("hauteur"));
        modal.find("#view_nectar").children("b").text(ligne.data("nectar"));
        modal.find("#view_pollen").children("b").text(ligne.data("pollen"));
        modal.find("#view_miellat").children("b").text(ligne.data("miellat"));
        modal.find("#view_floraisonCouleur").children("b").text(ligne.data("floraisoncouleur"));
        modal.find("#view_qrCode").children("b").text(ligne.data("qrcode"));
        modal.find("#view_qrCodeImg").attr("src", "../public/img/QR_Code/" + ligne.data("qrcode"));
    });
});