/*
==================
GESTION DES MODALS
==================
*/

// VARIABLES
let url_plantes = "plantes";
let url_users = "gestionUsagers";


/**
 * Gestion de la modification en base suivant le type
 * @param type
 */
function modifier(type) {
    let formData = new FormData($("#editModal form")[0]); // Parcours des inputs pour ajax
    let url = "";
    let success = null;
    let error = null;

    // Parcours des input pour refresh
    let params = {};
    $(".editForm input, .editForm select, .editForm textarea").each(function (index, element) {
        params[element.name] = element.value;
    });

    // Gestion de la modification suivant le type
    switch (type) {
        case "plante" :
            url = url_plantes;
            success = (data) => {
                switch (data) {
                    case "valid" :
                        $.notify("La plante a bien été modifié", "success");
                        refreshPlante(params);
                        $('#editModal').modal('hide');
                        break;
                    case "1" :
                        $.notify("La plante n'a pas été trouvé", "error");
                        break;
                    default :
                        $.notify("Erreur dans la saisie", "info");
                }
            };
            error = () => {
                $.notify("Impossible de modifier la plante", "error");
            };
            break;

        case "utilisateur" :
            url = url_users;
            success = (data) => {
                switch (data) {
                    case "valid" :
                        $.notify("L'utilisateur a bien été modifié", "success");
                        refreshUser(params);
                        $('#editModal').modal('hide');
                        break;
                    case "1" :
                        $.notify("Le compte n'a pas été trouvé", "error");
                        break;
                    case "2" :
                        $.notify("Le compte ne peut pas être modifié", "error");
                        break;
                    case "notsamepassword" :
                        $.notify("Les mots de passe ne sont pas identiques", "info");
                        break;
                    case "existuser" :
                        $.notify("Le nom d'utilisateur est déjà pris", "info");
                        break;
                    default :
                        $.notify("Erreur dans la saisie", "info");
                }
            };
            error = () => {
                $.notify("Impossible de modifier le compte", "error");
            };
            break;
    }

    $.ajax({
        url: url + "/edit",
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: success,
        error: error
    });
}


/**
 * Gestion de la creation en base suivant le type
 * @param type
 */
function ajouter(type) {
    let formData = new FormData($("#addModal form")[0]);
    let url = "";
    let success = null;
    let error = null;

    // Gestion de la modification suivant le type de modification
    switch (type) {
        case "plante" :
            url = url_plantes;
            success = (data) => {
                switch (data) {
                    case "valid" :
                        $.notify("La plante a bien été ajoutée", "success");
                        window.location.reload();
                        break;
                    default :
                        $.notify("Erreur dans la saisie", "info");
                }
            };
            error = () => {
                $.notify("Impossible d'ajouter la plante", "error");
            };
            break;

        case "utilisateur" :
            url = url_users;
            success = (data) => {
                switch (data) {
                    case "valid" :
                        $.notify("L'utilisateur a bien été ajouté", "success");
                        window.location.reload();
                        break;
                    case "notsamepassword" :
                        $.notify("Les mots de passe ne sont pas identiques", "info");
                        break;
                    case "existuser" :
                        $.notify("Le nom d'utilisateur est déjà pris", "info");
                        break;
                    default :
                        $.notify("Erreur dans la saisie", "info");
                }
            };
            error = () => {
                $.notify("Impossible d'ajouter l'utilisateur", "error");
            };
            break;
    }

    $.ajax({
        url: url + "/add",
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: success,
        error: error
    });
}


/**
 * Gestion de la suppression en base suivant le type
 * @param type
 */
function supprimer(type) {
    let modal = $("#removeModal");
    let ligneASup = null;
    let url = "";
    let success = null;
    let error = null;

    // Gestion de la modification suivant le type de modification
    switch (type) {
        case "plante" :
            ligneASup = $("tbody tr[data-id=" + modal.find('#r_plante_id').val() + "]");
            url = url_plantes;
            success = (data) => {
                if (data == 0) {
                    $.notify("La plante a bien été supprimée", "success");
                    ligneASup.remove();
                } else {
                    if (data == 1) {
                        $.notify("La plante n'a pas été trouvée", "error");
                    }
                }
            };
            error = () => {
                $.notify("Impossible de supprimer la plante", "error");
            };
            break;

        case "utilisateur" :
            ligneASup = $("tbody tr[data-id=" + modal.find('#r_idutilisateur').val() + "]");
            url = url_users;
            success = (data) => {
                if (data == 0) {
                    $.notify("Le compte a bien été supprimé", "success");
                    ligneASup.remove();
                } else {
                    if (data == 1) {
                        $.notify("Le compte n'a pas été trouvé", "error");
                    } else {
                        $.notify("Le compte ne peut pas être supprimé", "error");
                    }
                }
            };
            error = () => {
                $.notify("Impossible de supprimer le compte", "error");
            };
            break;
    }

    $.ajax({
        url: url + "/remove",
        type: 'POST',
        data: 'id=' + ligneASup.data("id"),
        success: success,
        error: error
    });
    modal.modal('hide');
}

$(document).ready(function () {
    $('table').DataTable({
        "language": {
            "lengthMenu": "Afficher _MENU_ par page",
            "zeroRecords": "Aucun résultat",
            "info": "Page _PAGE_ sur _PAGES_",
            "infoEmpty": "Aucun enregistrement",
            "infoFiltered": "(sur un total de _MAX_ enregistrements)"
        },
        paging: false,
        ordering: true,
        info: false,
        order: [[0, "desc"]]
    });

    // Affichage des tooltips
    $('[data-toggle="tooltip"]').tooltip();
});