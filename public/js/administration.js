/**
 * Gestion du click sur dossier
 * @param elementSelectionne element html du dossier select
 */
function gestionClickSurDossier(elementSelectionne) {
    let dossierSelect = $(elementSelectionne);

    // Gestion affichage dossier
    $(".dossierCourant").removeClass("dossierCourant");
    dossierSelect.addClass("dossierCourant");

    // Maj du contenu de l'iframe
    $("iframe").attr("src", dossierSelect.data("url"));
}

/**
 * Gestion listener
 */
$(document).ready(function () {
    // Acces dossier avec refresh de l'affichage
    $(".content_bottom__left li").on("click", function () {
        gestionClickSurDossier(this);
    });
});