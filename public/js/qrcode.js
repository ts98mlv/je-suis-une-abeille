$(document).ready(() => {
    $("body").css("background-color", "black");
    $("footer").css("color", "white");
    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function(content) {
        traitementResult(content);
    });
    Instascan.Camera.getCameras().then(function(cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function(e) {
        console.error(e);
    });
});

function traitementResult(content) {
    let tabDatas = content.split("/");
    let id = tabDatas[0];
    let data = tabDatas[1];
    if (id === "BeeChallenge") {
        if (data == "start" && !("starterCode" in localStorage)) {
            document.location.href = './game';
            localStorage.setItem("starterCode", true);
        } else if (data == "end" && (("BadEnd" in localStorage) || ("GoodEnd" in localStorage))) {
            document.location.href = './end';
        } else {
            console.log(data);
            if (!("starterCode" in localStorage)) {
                $(".message").text("Ceci n'est pas le Qr code de départ, merci de scanner le bon Qr code.")
            } else if (("BadEnd" in localStorage) || ("GoodEnd" in localStorage)) {
                $(".message").text("Ceci n'est pas le Qr code de fin, merci de scanner le bon Qr code.")
            } else if (data == "start" || data == "end") {
                $(".message").text("Ceci n'est pas un Qr code de plante, merci de scanner le bon Qr code.");
            } else {
                document.location.href = './game?qr=' + content;
            }
        }
    } else {
        $(".message").text("Ceci n'est pas un Qr code de BeeChallenge")
    }
}