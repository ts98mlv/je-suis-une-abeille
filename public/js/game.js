$(document).ready(() => {
    if (!("firstDone" in localStorage)) {
        $("#modalRegle").modal("show");
        localStorage.setItem("potDeMiel", 0);
        localStorage.setItem("nectar", 0);
        localStorage.setItem("pollen", 6);
        localStorage.setItem("nbPlantes", [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        localStorage.setItem("firstDone", true);
        localStorage.setItem("tabPlanteTrouvees", []);
        localStorage.setItem("nbPlantesTrouvees", 0);
    } else {
        if ($("#modalQrState")[0].dataset.qr !== "null" && $("#modalQrState")[0].dataset.qr !== "") {
            $("#titleQrResult").append("Félicitation, vous avez trouvé la plante:<strong>" + $("#modalQrState")[0].dataset.nomplante + "</strong>");
            console.log($("#modalQrState")[0].dataset);
            $("#qrResultBody").append('<p>' + $("#modalQrState")[0].dataset.qr + '</p>');
            $(".closeModalQRBtn").addClass("valid").removeClass("invalid");
            dataGameUpdate();
            $("#modalQrState").modal("show");
        } else if ($("#modalQrState")[0].dataset.qr !== "") {
            $("#titleQrResult").append("Désolé, ceci n'est pas un Qr code du jeu");
            $(".closeModalQRBtn").addClass("invalid").removeClass("valid");
            $("#modalQrState").modal("show");
        }
    }
    $("#status").click(() => {
        let plantesMemoire = localStorage.getItem("tabPlanteTrouvees").split(",");
        $("#statusNbPlante").text("Nombre de plante restantes a trouver : "+(plantesMemoire.length - 1));
        let plantesTrouves = localStorage.getItem("nbPlantes").split(",");
        let countZoneValidee = plantesTrouves.filter(x => x == '0').length;
        $("#statusZones").text("Nombre de zones du jardin à visiter restantes : "+countZoneValidee);
    });
    let nPotMiel = localStorage.getItem("potDeMiel");
    let nNectar = localStorage.getItem("nectar");
    let nPollen = localStorage.getItem("pollen");
    $("#nbPotMiel").append(nPotMiel);
    $("#nbNectar").append(nNectar);
    $("#nbPollen").append(nPollen);
    $(".closeModalQRBtn").click(() => {
        let plantesTrouves = localStorage.getItem("nbPlantes").split(",");
        if ($(".closeModalQRBtn").hasClass('valid') && !plantesTrouves.includes("0")) {
            console.log("end");
            localStorage.setItem("GoodEnd", "Mission trouver une plante mellifère par zone accomplie");
            redirectToEnd();
        }
    });
    if ("BadEnd" in localStorage || "GoodEnd" in localStorage) {
        redirectToEnd();
    }

    //console.log(localStorage.getItem("nbPlantes").split(","));
});

function redirectToEnd() {
    localStorage.removeItem("firstDone");
    $.ajax({
        url: "deleteTimer",
        method: 'DELETE',
        dataType: 'json',
        success: (response, xhr) => {
            console.log("delete success");
            document.location.href = "./scanEnd";
        },
        error: (xhr) => {
            console.log('data transfert error' + xhr);
        }
    });
}

function dataGameUpdate() {
    let plantesMemoire = localStorage.getItem("tabPlanteTrouvees").split(",");
    plantesMemoire.forEach((e, index) => { if (e !== "") plantesMemoire[index] = parseInt(e); });
    let idPlante = parseInt($("#modalQrState")[0].dataset.id);
    if (!plantesMemoire.includes(idPlante)) {
        plantesMemoire.push(idPlante);
        let plantesTrouves = localStorage.getItem("nbPlantes").split(",");
        let nbPlantesTrouvees = 1;
        plantesTrouves.forEach((e, index) => {
            plantesTrouves[index] = parseInt(e);
            nbPlantesTrouvees += plantesTrouves[index];
        });
        plantesTrouves[parseInt($("#modalQrState")[0].dataset.idzone) - 1] += 1;
        localStorage.setItem("nbPlantes", plantesTrouves);
        let currentPollen = parseInt(localStorage.getItem("pollen"));
        let nectarObtenu = Math.round((parseInt($("#modalQrState")[0].dataset.nectar) + parseInt(localStorage.getItem("nectar"))) * (1 + (currentPollen / 50)));
        let mielObtenu = parseInt(localStorage.getItem("potDeMiel"));
        let pollenObtenu = parseInt($("#modalQrState")[0].dataset.pollen) - 1;
        if (pollenObtenu > 10) {
            pollenObtenu = pollenObtenu / 2;
        }
        if (nectarObtenu > 10) {
            nectarObtenu -= 10;
            mielObtenu += 1;
        }
        localStorage.setItem("nbPlantesTrouvees", nbPlantesTrouvees);
        localStorage.setItem("potDeMiel", mielObtenu);
        localStorage.setItem("nectar", nectarObtenu);
        localStorage.setItem("pollen", currentPollen + pollenObtenu);
        localStorage.setItem("tabPlanteTrouvees", plantesMemoire);
    }
}