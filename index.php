<?php
require './vendor/autoload.php';
require __DIR__ . '/src/config/config.inc.php';

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use abeille\middlewares\EstDejaConnectUser;
use abeille\middlewares\EstConnectUser;
use abeille\middlewares\EstConnectAdmin;
use Slim\App;

session_start();
$container=array();
$container["settings"]=["displayErrorDetails"=>true];
$container["view"]=function($container){
    $views=new \Slim\Views\Twig('src/vue',[]);
    $router=$container->get('router');
    $uri=\Slim\Http\Uri::createFromEnvironment(new Slim\Http\Environment($_SERVER));
    $views->addExtension(new Slim\Views\TwigExtension($router,$uri));
    return $views;
};

///////////////
// ELOQUENT //
//////////////
$container['settings'] = $config;
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};

// Init Slim
$app = new App($container);

// Routes
$app->get("/","abeille\\controller\\MainController:afficherConnexion")->setName('getHome');//->add(new EstDejaConnectUser($app->getContainer()));
$app->get("/scan","abeille\\controller\\ScanController:getScan")->setName('getScan');//->add(new EstConnectUser($app->getContainer()));
$app->get("/game","abeille\\controller\\GameController:getGame")->setName('getGame');//->add(new EstConnectUser($app->getContainer()));
$app->get("/connexion", "abeille\\controller\\MainController:afficherConnexion")->setName("afficherConnexion");
$app->get("/creerCompte", "abeille\\controller\\MainController:afficherInscription")->setName("afficherInscription");
$app->post("/connexion", "abeille\\controller\\AccountController:connexion");
$app->post("/inscription", "abeille\\controller\\AccountController:inscription");
$app->get("/deconnexion", "abeille\\controller\\AccountController:deconnexion")->setName("deconnexion");


$app->get("/scanPlante","abeille\\controller\\ScanController:getScanPlante")->setName('getScanPlante');//->add(new EstConnectUser($app->getContainer()));
$app->get("/scanEnd","abeille\\controller\\ScanController:getScanEnd")->setName('getEnd');//->add(new EstConnectUser($app->getContainer()));
$app->get("/end","abeille\\controller\\EndGameController:getEnd")->setName('getEnd');//->add(new EstConnectUser($app->getContainer()));
$app->post("/startTimer","abeille\\controller\\TimerController:startTimer")->setName('startTimer');//->add(new EstConnectUser($app->getContainer()));
$app->get("/getTimer","abeille\\controller\\TimerController:getTimer")->setName('getTimer');//->add(new EstConnectUser($app->getContainer()));
$app->delete("/deleteTimer","abeille\\controller\\TimerController:deleteTimer")->setName('deleteTimer');//->add(new EstConnectUser($app->getContainer()));

// Administration
$app->get("/administration","abeille\\controller\\AdministrationController:getAdministration")->setName('getAdministration')->add(new EstConnectAdmin($app->getContainer()));
$app->get("/administration/plantes","abeille\\controller\\AdministrationController:getAdministrationPlantes")->setName('getAdministrationPlantes')->add(new EstConnectAdmin($app->getContainer()));
$app->post("/administration/plantes/remove","abeille\\controller\\AdministrationController:supprimerPlante")->add(new EstConnectAdmin($app->getContainer()));
$app->post("/administration/plantes/add","abeille\\controller\\AdministrationController:ajouterPlante")->add(new EstConnectAdmin($app->getContainer()));
$app->post("/administration/plantes/edit","abeille\\controller\\AdministrationController:modifierPlante")->add(new EstConnectAdmin($app->getContainer()));


$app->get("/administration/importCSV", "abeille\\controller\\AdministrationController:importerPlanteByCSV")->setName('importCSV')->add(new EstConnectAdmin($app->getContainer()));
$app->post("/administration/importCsv", "abeille\\controller\\AdministrationController:importPlantByCsv")->add(new EstConnectAdmin($app->getContainer()));
$app->get("/administration/gestionUsagers", "abeille\\controller\\AdministrationController:afficherGestionUsagers")->setName("gestionUsagers")->add(new EstConnectAdmin($app->getContainer()));
$app->post("/administration/gestionUsagers/remove", "abeille\\controller\\AdministrationController:supprimerCompte")->add(new EstConnectAdmin($app->getContainer()));
$app->post("/administration/gestionUsagers/edit", "abeille\\controller\\AdministrationController:modifierCompte")->add(new EstConnectAdmin($app->getContainer()));
$app->post("/administration/gestionUsagers/add", "abeille\\controller\\AdministrationController:ajouterCompte")->add(new EstConnectAdmin($app->getContainer()));

$app->get("/administration/monProfile", "abeille\\controller\\AccountController:afficherMonProfil")->setName("monProfile")->add(new EstConnectAdmin($app->getContainer()));
$app->post("/modifierProfil", "abeille\\controller\\AccountController:modifierProfil")->add(new EstConnectAdmin($app->getContainer()));

$app->run();