<?php


namespace abeille\model;


use Illuminate\Database\Eloquent\SoftDeletes;

class Zone extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'ZONE';
    protected $primaryKey = 'zone_id';
    public $timestamps = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}