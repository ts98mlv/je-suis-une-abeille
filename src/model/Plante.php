<?php


namespace abeille\model;


use Illuminate\Database\Eloquent\SoftDeletes;

class Plante extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'PLANTE';
    protected $primaryKey = 'plante_id';
    public $timestamps = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}