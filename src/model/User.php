<?php


namespace abeille\model;


use Illuminate\Database\Eloquent\SoftDeletes;

class User extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'USER';
    protected $primaryKey = 'user_id';
    public $timestamps = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}