<?php

namespace abeille\middlewares;

use abeille\controller\MainController;
use Psr\Http\Message\ResponseInterface;

/**
 * Class EstDejaConnectUser
 * Si l'user est deja co, on empeche tout acces a des pages comme login et inscription et on redirige vers le jeu
 * @package abeille\middlewares
 */
class EstDejaConnectUser extends MainController
{

    /**
     * méthode invoquée lors de l'utilisation du middleware
     * @param $request
     * @param $response
     * @param $next
     * @return ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {

        // Controle si user connecte
        if (isset($_SESSION['user'])) {
            return $this->redirect($response, 'getGame');
        }

        $response = $next($request, $response);

        return $response;
    }
}