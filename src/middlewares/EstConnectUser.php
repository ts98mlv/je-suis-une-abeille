<?php

namespace abeille\middlewares;

use abeille\controller\MainController;
use Psr\Http\Message\ResponseInterface;

/**
 * Class EstConnectUser
 * Si l'user n'est pas co, on empeche tout acces et on redirige vers page home
 * @package abeille\middlewares
 */
class EstConnectUser extends MainController
{

    /**
     * méthode invoquée lors de l'utilisation du middleware
     * @param $request
     * @param $response
     * @param $next
     * @return ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {

        // Controle si user non connecte
        if (!isset($_SESSION['user']) || empty($_SESSION['user'])) {
            return $this->redirect($response, 'getHome');
        }

        $response = $next($request, $response);

        return $response;
    }
}