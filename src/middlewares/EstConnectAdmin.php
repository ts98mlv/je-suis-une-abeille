<?php

namespace abeille\middlewares;

use abeille\controller\MainController;
use Psr\Http\Message\ResponseInterface;

/**
 * Class EstConnectUser
 * Acces au page seulement pour les admins
 * @package abeille\middlewares
 */
class EstConnectAdmin extends MainController
{

    /**
     * méthode invoquée lors de l'utilisation du middleware
     * @param $request
     * @param $response
     * @param $next
     * @return ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        if (isset($_SESSION['user'])) {
            if ($_SESSION['user']["level"] != 1) {
                return $this->redirect($response, 'getGame');
            } else {
                $response = $next($request, $response);
                return $response;
            }
        } else {
            return $this->redirect($response, 'getHome');
        }
    }
}