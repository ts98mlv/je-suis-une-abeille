<?php

namespace abeille\controller;

class MainController
{
    // Container de Slim
    private $container;

    function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Getter
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->container->get($name);
    }

    /**
     * Render function
     * @param $response
     * @param $view
     * @param array $params
     * @return
     */
    public function render($response, $view, $params = [])
    {
        return $this->view->render($response, $view, $params);
    }

    /**
     * Redirect function avec nom de la route
     * @param  $response
     * @param $name String name of slim route
     * @return
     */
    public function redirect($response, $name)
    {
        return $response->withStatus(301)->withHeader('Location', $this->router->pathFor($name));
    }

    /**
     * méthode qui permet d'afficher la page de connexion
     * @param $request
     * @param $response
     * @return mixed
     */
    public function afficherConnexion($request, $response){
        return $this->render($response, "connexion.html.twig");
    }

    /**
     * méthode qui permet d'afficher la page d'inscription
     * @param $request
     * @param $response
     * @return mixed
     */
    public function afficherInscription($request, $response){
        return $this->render($response, "inscription.html.twig");
    }

    /**
     * Filtrage des donnees envoye par l'utilisateur
     */
    public function filtrer_valeur_post()
    {
        foreach ($_POST as $cle => $value) {
            if (is_array($value)) {
                foreach ($value as $subKey => $subValue) {
                    $_POST[$cle][$subKey] = strip_tags($subValue);
                }
            } else {
                $_POST[$cle] = strip_tags($value);
            }
        }
    }
}