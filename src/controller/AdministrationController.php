<?php

namespace abeille\controller;

use abeille\model\Plante;
use abeille\model\User;
use abeille\model\Zone;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

require(__DIR__ . "/../lib/phpqrcode/qrlib.php");

class AdministrationController extends MainController
{

    public function getAdministration($request, $response)
    {
        return $this->render($response, "administration.html.twig");
    }

    public function getAdministrationPlantes($request, $response)
    {
        $zones = Zone::select("nom")->get();
        $params = ["plantes" => Plante::join("ZONE", "PLANTE.zone_id", "=", "ZONE.zone_id")->get(), "zones" => $zones];
        return $this->render($response, "administration/administration_plantes.html.twig", $params);
    }

    public function importerPlanteByCSV($request, $response)
    {
        return $this->render($response, "administration/administration_importCsv.html.twig");
    }

    public function ajouterZone($nom)
    {
        $zone = new Zone();
        $zone->nom = $nom;
        $zone->save();

        return $zone->zone_id;
    }

    public function importPlantByCsv($request, $response)
    {

        //Process the CSV file
        $handle = fopen($_FILES['csv']['tmp_name'], "r");
        $data = fgetcsv($handle, 1000, ";"); //Remove if CSV file does not have column headings
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            $plante = new Plante();
            $plante->nomLatin = $data[0];
            $plante->nomFrancais = $data[1];

            $zone = Zone::where("nom", "like", "%" . $data[7] . "%")->first();
            if (!isset($zone) || empty($zone)) {
                $zone = new Zone();
                $zone->nom = $data[7];
                $zone->save();
            }
            $plante->zone_id = $zone->zone_id;
            unset($zone);
            $plante->hauteur = $data[2];
            $plante->nectar = $data[3];
            $plante->pollen = $data[4];
            if ($data[5] != "")
                $plante->miellat = 1;
            else
                $plante->miellat = 0;
            $plante->floraisonCouleur = $data[6];
            $plante->save();

            //generation du QRcode
            $qrcode = $this->genererQrCode($plante->plante_id, "BeeChallenge/");
            $plante->qrCode = $qrcode;
            $plante->save();
        }

        //redirection
        return $this->redirect($response, "getAdministrationPlantes");
    }

    public function genererQrCode($id, $identifieurAppli)
    {
        $dir = __DIR__ . "/../../public/img/QR_Code";
        $filename = "qr_plante_" . $id . ".png";

        $path = $dir . "/" . $filename;

        if (!file_exists($path)) {
            \QRcode::png($identifieurAppli . $id, $path);
        }

        return $filename;
    }

    public function afficherGestionUsagers($req, $res)
    {
        $users = User::all();
        return $this->render($res, "administration/administration_utilisateurs.html.twig", ["users" => $users]);
    }

    public function supprimerPlante(RequestInterface $request, ResponseInterface $response)
    {
        $plante = Plante::find($_POST['id']);
        $body = $response->getBody();
        if (empty($plante)) {
            return $body->write('1');
        } else {
            $plante->delete();
            return $body->write('0');
        }
    }

    public function ajouterPlante(RequestInterface $request, ResponseInterface $response)
    {
        // Filtrage des donnees utilisateur
        $this->filtrer_valeur_post();
        $body = $response->getBody();

        // Creation de la plante
        $plante = new Plante();
        $plante->nomFrancais = $_POST['nomFrancais'];
        $plante->nomLatin = $_POST['nomLatin'];

        // Recup ID zone
        $zone_id = Zone::where("nom", "=", $_POST['zone'])->get()[0]->zone_id;
        $plante->zone_id = $zone_id;

        $plante->description = $_POST['description'];
        $plante->hauteur = $_POST['hauteur'];
        $plante->nectar = $_POST['nectar'];
        $plante->pollen = $_POST['pollen'];
        $plante->miellat = $_POST['miellat'];
        $plante->floraisonCouleur = $_POST['floraisonCouleur'];
        $plante->save();

        // Generation QRCode
        $qrcode = $this->genererQrCode($plante->plante_id, "BeeChallenge/");
        $plante->qrCode = $qrcode;
        $plante->save();

        return $body->write("valid");
    }

    public function modifierPlante(RequestInterface $request, ResponseInterface $response)
    {
        // Filtrage des donnees utilisateur
        $this->filtrer_valeur_post();
        $body = $response->getBody();

        $plante = Plante::find($_POST['plante_id']);

        if ($plante == []) {
            return $body->write("1");
        }

        $plante->nomFrancais = $_POST['nomFrancais'];
        $plante->nomLatin = $_POST['nomLatin'];

        // Recup ID zone
        $zone_id = Zone::where("nom", "=", $_POST['zone'])->get()[0]->zone_id;
        $plante->zone_id = $zone_id;

        $plante->description = $_POST['description'];
        $plante->hauteur = $_POST['hauteur'];
        $plante->nectar = $_POST['nectar'];
        $plante->pollen = $_POST['pollen'];
        $plante->miellat = $_POST['miellat'];
        $plante->floraisonCouleur = $_POST['floraisonCouleur'];
        $plante->save();

        return $body->write("valid");
    }

    public function supprimerCompte(RequestInterface $request, ResponseInterface $response)
    {
        $user = User::find($_POST['id']);
        $body = $response->getBody();
        if (empty($user)) {
            return $body->write('1');
        } else {
            if ($_POST['id'] == 1) {
                return $body->write('2');
            } else {
                $user->delete();
                return $body->write('0');
            }
        }
    }

    public function modifierCompte(RequestInterface $request, ResponseInterface $response)
    {
        $this->filtrer_valeur_post();
        $user = User::find($_POST['idutilisateur']);
        $body = $response->getBody();
        if (empty($user)) {
            return $body->write('1');
        } else {
            if ($_POST['idutilisateur'] == 1) {
                return $body->write('2');
            } else {
                $reponse = $this->modificationCompte($_POST, $user, 1);
                return $body->write($reponse);
            }
        }
    }

    public function modificationCompte($new, $old, $panelAdmin = 0)
    {
        if ($new['password'] != $new['passwordrepeat']) {
            return "notsamepassword";
        } else {
            if (strlen($new['username']) > 50 || strlen($new['username']) < 1) {
                return "usernamesize";
            } else {
                if (strlen($new['firstname']) > 50 || strlen($new['firstname']) < 1) {
                    return "firstnamesize";
                } else {
                    if (strlen($new['lastname']) > 50 || strlen($new['lastname']) < 1) {
                        return "lastnamesize";
                    } else {
                        if (strlen($new['password']) > 50 || strlen($new['password']) < 5) {
                            return "passwordsize";
                        } else {
                            if (strlen($new['email']) > 200 || strlen($new['email']) < 1) {
                                return "emailsize";
                            } else {
                                $user = User::where('login', $new['username'])->first();
                                // Test si nom user non pris
                                if ($user == [] || $new['username'] == $old->login) {
                                    $user = User::find($old->user_id);
                                    // Verification si mdp touche ou non
                                    if ($new['password'] != "password") {
                                        // Hash du mot de passe
                                        $hash = password_hash($new['password'], PASSWORD_DEFAULT);
                                        $user->password = $hash;
                                    }
                                    $user->login = $new['username'];
                                    $user->nom = $new['lastname'];
                                    $user->prenom = $new['firstname'];
                                    $user->email = $new['email'];

                                    if ($panelAdmin == 0) {
                                        $_SESSION['user']->login = $new['username'];
                                        $_SESSION['user']->nom = $new['lastname'];
                                        $_SESSION['user']->prenom = $new['firstname'];
                                    } elseif ($panelAdmin == 1) {
                                        $user->level = $new['admin'];
                                    }

                                    $user->save();
                                    return "valid";
                                } else {
                                    return "existuser";
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function ajouterCompte(RequestInterface $request, ResponseInterface $response)
    {
        $this->filtrer_valeur_post();
        $reponse = $this->addAccount($_POST, 1);
        $body = $response->getBody();
        return $body->write($reponse);
    }


    public function addAccount($post, $panelAdmin = 0)
    {
        if ($post['password'] != $post['passwordrepeat']) {
            return "notsamepassword";
        } else {
            if (strlen($post['username']) > 50 || strlen($post['username']) < 1) {
                return "usernamesize";
            } else {
                if (strlen($post['firstname']) > 50 || strlen($post['firstname']) < 1) {
                    return "firstnamesize";
                } else {
                    if (strlen($post['lastname']) > 50 || strlen($post['lastname']) < 1) {
                        return "lastnamesize";
                    } else {
                        if (strlen($post['password']) > 50 || strlen($post['password']) < 5) {
                            return "passwordsize";
                        } else {
                            if (strlen($post['email']) > 200 || strlen($post['email']) < 1) {
                                return "emailsize";
                            } else {
                                $user = User::where('login', $post['username'])->first();
                                // Test si nom user non pris
                                if ($user == []) {
                                    $user = new User();
                                    $user->login = $post['username'];
                                    $user->nom = $post['lastname'];
                                    $user->prenom = $post['firstname'];
                                    $user->email = $post['email'];
                                    // Hash du mot de passe
                                    $hash = password_hash($post['password'], PASSWORD_DEFAULT);
                                    $user->password = $hash;
                                    if ($panelAdmin == 1) {
                                        $user->level = $post['admin'];
                                    }
                                    $user->save();

                                    return "valid";
                                } else {
                                    return "existuser";
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}