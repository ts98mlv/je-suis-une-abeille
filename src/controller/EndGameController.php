<?php
namespace abeille\controller;


class EndGameController extends MainController{
   
    public function getEnd($request, $response){
        return $this->render($response,"endGame.html.twig",[]);
    }
    
}