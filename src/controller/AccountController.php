<?php
namespace abeille\controller;

use abeille\model\User;

class AccountController extends MainController{
   
    public function getHome($request, $response){
        return $this->render($response,"home.html.twig");
    }


    public function connexion($request, $response){

        try{
            //recuperation des donnees du post
            $login = (!empty($_POST["login"])) ? $_POST["login"] : null;
            $m2p = (!empty($_POST["m2p"])) ? $_POST["m2p"] : null;
            $login = filter_var($login, FILTER_SANITIZE_STRING);

            //verification du remplissage de tous les champs
            if(! isset($login) || ! isset($m2p))
                throw new \Exception("un champ requis n'a pas été rempli");

            //récupération de l'utilisateur si possible
            $user = User::where("login", "=", $login)
                ->orWhere("email", "=", $login)
                ->first();

            if(!isset($user)) throw new \Exception("Aucun utilisateur ne correspond à ce login");

            //vérification de la conformité du mot de passe

            if(!password_verify($m2p, $user->password)) throw new \Exception("Mot de passe incorrect");

            //enregistrement en session
            if(isset($_SESSION["user"])) unset($_SESSION["user"]);
            $_SESSION["user"] = [
                "id" => $user->user_id,
                "level" => $user->level
            ];

            //redirection en fonction des droits
            if($user->level == 0){ // cas joueur
                return $this->redirect($response, "getScan");
            }elseif ($user->level == 1){ //cas admin
                return $this->redirect($response, "getAdministration");
            }


        }catch (\Exception $e){
            die($e->getMessage());
        }

    }

    public function inscription($request, $response){
        try{
            //recuperation des donnees du post
            $email = (!empty($_POST["email"])) ? $_POST["email"] : null;
            $login = (!empty($_POST["login"])) ? $_POST["login"] : null;
            $nom = (!empty($_POST["nom"])) ? $_POST["nom"] : null;
            $prenom = (!empty($_POST["prenom"])) ? $_POST["prenom"] : null;
            $m2p = (!empty($_POST["m2p"])) ? $_POST["m2p"] : null;
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            $login = filter_var($login, FILTER_SANITIZE_STRING);
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);

            //verification du remplissage de tous les champs
            if(!isset($email) || ! isset($login) || ! isset($nom) || ! isset($prenom) || ! isset($m2p))
                throw new \Exception("un champ requis n'a pas été rempli");

            //insertion de l'utilisateur en bdd
            $user = new User();
            $user->email = $email;
            $user->login = $login;
            $user->prenom = $prenom;
            $user->nom = $nom;
            $user->password = password_hash($m2p, PASSWORD_DEFAULT);
            $user->level = 0;

            $user->save();

            //redirection
            return $this->redirect($response, "getHome");


        }catch (\Exception $e){
            die($e->getMessage());
        }
    }

    /**
     * méthode qui permet la déconnexion d'un utilisateur
     * @param $request
     * @param $response
     * @return mixed
     */
    public function deconnexion($request, $response){
        try{
            if(isset($_SESSION['user'])) unset ($_SESSION["user"]);

            //redirection
            return $this->redirect($response, "getHome");
        }catch(\Exception $e){
            die($e->getMessage());
        }
    }

    /**
     * méthode qui permet de supprimer un compte utilisateur
     * @param $request
     * @param $response
     */
    public function supprimerCompte($request, $response, $args){
        $id = $args["id"];
        $retour = false;
        if(isset($id) && $id != ""){
            $compte = User::where("user_id", "=", $id);
            if(isset($compte)){
                $compte->delete();
                $retour = true;
            }
        }

        return $retour;
    }

    /**
     * methode qui permet d'afficher la page de modification du profil
     * @param $request
     * @param $response
     * @return mixed
     */
    public function afficherMonProfil($request, $response){
        $user = User::find($_SESSION[user]["id"]);
        return $this->render($response, "administration/administration_monProfile.html.twig", ["user" => $user]);
    }

    public function modifierProfil($request, $response){
        try{
            //recupéraation des données
            $nom = (isset($_POST['nom'])) ? $_POST['nom'] : null;
            $prenom = (isset($_POST['prenom'])) ? $_POST['prenom'] : null;
            $pseudo = (isset($_POST['pseudo'])) ? $_POST['pseudo'] : null;
            $email = (isset($_POST['email'])) ? $_POST['email'] : null;
            $m2p = (isset($_POST['m2p'])) ? $_POST['m2p'] : null;
            if($m2p == "") $m2p = null;
            $id = (isset($_POST['id'])) ? $_POST['id'] : null;

            //verification des champs requis
            if(!isset($nom) || !isset($prenom) || !isset($pseudo) || !isset($email)){
                throw new \Exception("un champs requis n'a pas été rempli");
            }

            //modif du compte
            $user = User::find($id);
            if(!isset($user)) throw new \Exception("utilisateur inconnu");

            $user->nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $user->prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
            $user->login = filter_var($pseudo, FILTER_SANITIZE_STRING);
            $user->email = filter_var($email, FILTER_SANITIZE_EMAIL);

            if(isset($m2p)) $user->password = password_hash($m2p, PASSWORD_DEFAULT);

            $user->save();
        }catch(\Exception $e){die($e->getMessage());}
        return $this->redirect($response, "getAdministration");
    }
}