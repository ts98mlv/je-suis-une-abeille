<?php
namespace abeille\controller;
use abeille\model\Plante;

class GameController extends MainController{
   
    public function getGame($request, $response){
        $qr=null;
        $plante=new Plante();
        if(isset($_GET["qr"])){
            $qr=$_GET["qr"];
            $data=explode('/',$qr);
            $plante=Plante::find($data[1]);
        }
        return $this->render($response,"game.html.twig",['qr'=>$qr,"nom"=>$plante->nomFrancais,'idPlante'=>$plante->plante_id,'idZone'=>$plante->zone_id,"pollen"=>$plante->pollen,"nectar"=>$plante->nectar]);
    }
    
}