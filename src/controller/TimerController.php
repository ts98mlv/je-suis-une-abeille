<?php
namespace abeille\controller;


class TimerController extends MainController{
   
    public function startTimer($request,$response){
        $_SESSION["timer"]=(time()*1000) + ((60 * 60) + (30 * 60)) * 1000;
        return json_encode(["code"=>200,"timer"=>$_SESSION["timer"]]);
    }
    public function getTimer($request,$response){
        return json_encode($_SESSION['timer']);
    
    }
    public function deleteTimer($request,$response){
        return json_encode(session_destroy());
    }
}