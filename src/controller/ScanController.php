<?php
namespace abeille\controller;


class ScanController extends MainController{
   
    public function getScan($request,$response){
        return $this->render($response,"scan.html.twig",['complement'=>"de départ pour commencer la partie"]);
    
    }
    public function getScanPlante($request,$response){
        return $this->render($response,"scan.html.twig",['complement'=>""]);
    
    }
    public function getScanEnd($request,$response){
        return $this->render($response,"scan.html.twig",['complement'=>"de fin pour terminer la partie"]);
    
    }
    
}