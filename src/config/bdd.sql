CREATE DATABASE IF NOT EXISTS `abeille`;
use `abeille`;

CREATE TABLE IF NOT EXISTS USER(
  user_id int(50) AUTO_INCREMENT,
  login varchar(50),
  password varchar(500),
  level int(5),
  email varchar(200) unique,
  nom varchar(50),
  prenom varchar(50),
  deleted_at datetime,
  PRIMARY KEY (user_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS ZONE(
    zone_id int(50) AUTO_INCREMENT,
    nom text,
    deleted_at datetime,
    PRIMARY KEY (zone_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS PLANTE(
  plante_id int(50) AUTO_INCREMENT,
  nomLatin varchar(200),
  nomFrancais varchar(200),
  zone_id int(50),
  description text,
  qrCode varchar(200),
  hauteur varchar(100),
  nectar varchar(100),
  pollen varchar(100),
  miellat int(2),
  floraisonCouleur text,
  photo varchar(200),
  deleted_at datetime,
  FOREIGN KEY (zone_id) REFERENCES ZONE(zone_id),
  PRIMARY KEY (plante_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
